﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(GenerateDither))]
[ExecuteInEditMode]
public class ApplyDither : MonoBehaviour {
    GenerateDither gen;
    new Renderer renderer;

	public void Apply() {
        renderer = gameObject.GetComponent<Renderer>();
        Material sharedMat = renderer.material;
        gen = gameObject.GetComponent<GenerateDither>();
        //Passes the albedo to the shader 
        sharedMat.SetTexture("_RefTex", gen.outputTex);
        //Pass the pallate and pallate info
        sharedMat.SetTexture("_DitherTextures", gen.ditherPallate);
        sharedMat.SetInt("_DitherDepth", gen.ditherPallate.depth);
        //Pass the color pallate to the shader
        sharedMat.SetTexture("_Pallate", gen.pallateTex);
        Color[] test = gen.pallateTex.GetPixels();
    }
}
