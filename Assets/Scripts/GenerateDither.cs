﻿using Eppy;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(ApplyDither))]
public class GenerateDither : MonoBehaviour {
    public Color[] pallateColors;
    public float alphaCutoff = 0.01f;
    [HideInInspector]
    public Texture2D outputTex;
    [HideInInspector]
    public Texture2D pallateTex;
    public Texture2DArray ditherPallate;
    [HideInInspector]
    public int maxPallateNumbers;
    //Original input color is the key, and the value is (PallateIndex1, PallateIndex2, DitherSampleNumber)
    private Dictionary<Color, int[]> dict;
    private Texture2D tempTex;

    public void GeneratePallate() {
        tempTex = GenerateTempTex();

        pallateColors = ColorClusters(GenerateColorPallate(tempTex));
    }


    public void Generate() {
        //Reset cached info
        pallateTex = null;
        dict = new Dictionary<Color, int[]>();
        tempTex = GenerateTempTex();

        Color[] pixels = tempTex.GetPixels();
        //Indexes pixel colors so that each color is only computed once 
        for (int i = 0; i < pixels.Length; i++) {
            Color p = pixels[i];
            if (p.a <= alphaCutoff)
                continue;
            if (!dict.ContainsKey(p.linear)) {
                dict.Add(p.linear, new int[3]);
            }
        }
        Color[] keys = dict.Keys.ToArray();
        for (int d = 0; d < keys.Length; d++) {
            Color i = keys[d];
            //2 color indexes + dither value + distance to true color
            Tuple<int, int, int, float> colorInfo = null;

            float minDistance = float.PositiveInfinity;
            int smallestIndex = int.MaxValue;

            //Determine primary color
            for (int p = 0; p < pallateColors.Length; p++) {
                Color iColor = pallateColors[p];
                float distance = ColorDistance(iColor, i.gamma);
                if (distance < minDistance) {
                    minDistance = distance;
                    smallestIndex = p;
                }
            }

            //Determine secondary color
            minDistance = float.PositiveInfinity;
            for (int p2 = 0; p2 < pallateColors.Length; p2++) {
                if (p2 == smallestIndex)
                    continue;
                Color iColor2 = pallateColors[p2];
                for (int s = 0; s < ditherPallate.depth; s++) {
                    Color comboColor = Color.Lerp(pallateColors[smallestIndex], iColor2, ((float)s / ((float)ditherPallate.depth)));

                    float distance = ColorDistance(comboColor, i.gamma);
                    if (distance < minDistance) {
                        minDistance = distance;
                        var tup = new Tuple<int, int, int, float>(smallestIndex, p2, s, i.a);
                        colorInfo = tup;
                    }else {
                        //Moving away; break.
                        break;
                    }
                }
            }

            dict[i] = new int[] { colorInfo.Item1, colorInfo.Item2, colorInfo.Item3, Mathf.RoundToInt(colorInfo.Item4 * (float)256) };
        }
        outputTex = GenerateTex(tempTex, dict);
        pallateTex = GeneratePallate(pallateColors);
        //ditherPallate = GenerateDitherArray(ditherSamples);
    }

    private Texture2D GenerateTempTex() {
        //Get the texture from the material
        Renderer rend = gameObject.GetComponent<Renderer>();
        Material sharedMat = rend.material;
        Texture2D texIn = (Texture2D)sharedMat.GetTexture("_MainTex");
        RenderTexture temp = RenderTexture.GetTemporary(
                            texIn.width,
                            texIn.height,
                            0,
                            RenderTextureFormat.Default,
                            RenderTextureReadWrite.Linear);
        // Blit the pixels on texture to the RenderTexture
        Graphics.Blit(texIn, temp);
        // Backup the currently set RenderTexture
        RenderTexture previous = RenderTexture.active;
        // Set the current RenderTexture to the temporary one we created
        RenderTexture.active = temp;
        // Create a new readable Texture2D to copy the pixels to it
        Texture2D newTex = new Texture2D(texIn.width, texIn.height);
        newTex.ReadPixels(new Rect(0, 0, temp.width, temp.height), 0, 0);
        // Reset the active RenderTexture
        RenderTexture.active = previous;
        newTex.filterMode = texIn.filterMode;
        newTex.Apply();
        // Release the temporary RenderTexture
        RenderTexture.ReleaseTemporary(temp);
        return newTex;
    }


    private Texture2D GenerateTex(Texture2D input, Dictionary<Color, int[]> infoDictionary) {
        Texture2D output = new Texture2D(input.width, input.height, TextureFormat.ARGB32, false);
        Color[] colors = new Color[input.height * input.width];
        for (int x = 0; x < input.height; x++) {
            for (int y = 0; y < input.width; y++) {
                Color current = input.GetPixel(x, y);
                if (current.a <= alphaCutoff)
                    continue;
                int[] pixelInfo = infoDictionary[current.linear];
                Color newCol = new Color(((pixelInfo[0]) / ((float)pallateColors.Length)), ((pixelInfo[1]) / ((float)pallateColors.Length)), (pixelInfo[2] / (float)256), Mathf.RoundToInt(pixelInfo[3] / (float)256));
                colors[x + (y * input.height)] = newCol;
                
            }
        }
        output.SetPixels(colors);
        output.Apply();
        output.filterMode = FilterMode.Point;
        return output;
    }

    private Texture2D GeneratePallate(Color[] pallate) {
        Texture2D output = new Texture2D(pallate.Length, 1, TextureFormat.ARGB32, false);
        for (int i = 0; i < pallate.Length; i++) {
            output.SetPixel(i, 0, pallate[i]);
        }
        output.Apply();
        output.filterMode = FilterMode.Point;
        output.wrapMode = TextureWrapMode.Repeat;
        return output;
    }

    

    private float ColorDistance(Color c1, Color c2) {

        LABColor l1 = LABColor.FromColor(c1);
        LABColor l2 = LABColor.FromColor(c2);
        return LABColor.Distance(l1, l2);
        //return (Mathf.Pow(v1.x + v2.x, 2) + Mathf.Pow(v1.y + v2.y, 2) + Mathf.Pow(v1.z + v2.z, 2));
    }

    private Vector3[] GenerateColorPallate(Texture2D texture) {

        Color[] pixels = texture.GetPixels();
        Vector3[] returned = new Vector3[pixels.Length];
        for (int i = 0; i < pixels.Length; i++) {
            Color color = pixels[i];
            float h;
            float s;
            float v;
            Color.RGBToHSV(color, out h, out s, out v);
            if (pixels[i].a <= alphaCutoff)
                h = -1f;
            returned[i] = new Vector3(h, s, v);
        }
        return returned;
    }

    private Color[] ColorClusters(Vector3[] imageColors) {
        var hueDict = new Dictionary<int, List<Vector2>>();
        int[] hues = new int[256];
        int[] hueClusters = new int[256];

        // build hue histogram.
        foreach (var hsvalue in imageColors) {
            if (hsvalue.x < 0)
                continue;

            hues[(int)(hsvalue.x * 256)]++;
            if (!hueDict.ContainsKey(Mathf.RoundToInt(hsvalue.x * 256))) {
                List<Vector2> newList = new List<Vector2>();
                newList.Add(new Vector2(hsvalue.y, hsvalue.z));
                hueDict.Add(Mathf.RoundToInt(hsvalue.x * 256), newList);
            } else {
                hueDict[Mathf.RoundToInt(hsvalue.x * 256)].Add(new Vector2(hsvalue.y, hsvalue.z));
            }
        }
        // calculate counts for clusters of colors.
        for (int i = 0; i < hues.Length; i++) {
            int huecluster = 0;
            for (int count = 0, j = i; count < 9; count++, j++) {
                huecluster += hues[j % hues.Length];
            }

            hueClusters[(i + 5) % hues.Length] = huecluster;
        }
        ArrayList colorsOut = new ArrayList();
        //Reduce number of entries based on averages
        ArrayList huesList = new ArrayList();
        int max = 0;
        for (int i = 0; i < hueClusters.Length; i++) {
            if (max < hueClusters[i])
                max = hueClusters[i];
        }
        ArrayList finalHues = new ArrayList();
        for (int i = 0; i < hueClusters.Length; i++) {
            //Debug.Log(hueclusters[i]);
            if (hueClusters[i] > max / 10) {
                finalHues.Add(i);
            }
        }
        for (int u = 0; u < finalHues.Count; u++) {
            int i = (int)finalHues[u];
            if (hueDict.ContainsKey(i)) {
                List<Vector2> sv = hueDict[i];
                Vector2 aggregate = Vector2.zero;
                foreach (Vector2 v in sv)
                    aggregate += v;
                aggregate /= (sv.Count);
                colorsOut.Add(Color.HSVToRGB(i / (float)256, aggregate.x, aggregate.y));
            }
        }


        Color[] colors = (Color[])colorsOut.ToArray(typeof(Color));
        ArrayList finalColors = new ArrayList();
        foreach (Color c in colors) {
            bool failed = false;
            foreach (Color c2 in finalColors) {
                if (ColorDistance(c, c2) < 0.2f)
                    failed = true;
            }
            if (!failed)
                finalColors.Add(c);
        }

        if (maxPallateNumbers != float.PositiveInfinity) {
            int iters = 0;
            while (iters < 100) {
                if (finalColors.Count <= maxPallateNumbers)
                    break;
                Color colorToRemove = new Color();
                float minDistance = float.PositiveInfinity;
                foreach (Color c in finalColors) {
                    //float h, s, v = 0;
                    //Color.RGBToHSV(c, out h, out s, out v);
                    foreach (Color c2 in finalColors) {
                        //float h2, s2, v2 = 0;
                        //Color.RGBToHSV(c, out h2, out s2, out v2);
                        //float hueDistance = Mathf.Min(Mathf.Abs(h - h2), 255 - Mathf.Abs(h - h2));
                        //if (hueDistance < minDistance) {
                        //    colorToRemove = c;
                        //    minDistance = hueDistance;
                        //}
                        float distance = ColorDistance(c, c2);
                        if (distance < minDistance) {
                            colorToRemove = c;
                            minDistance = distance;
                        }
                    }
                }
                finalColors.Remove(colorToRemove);
                iters++;

            }
        }

        return (Color[])finalColors.ToArray(typeof(Color));
    }
}
