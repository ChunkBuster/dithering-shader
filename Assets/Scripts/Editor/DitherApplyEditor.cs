﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CanEditMultipleObjects]
[CustomEditor(typeof(ApplyDither))]
public class DitherApplyEditor : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        ApplyDither dither = (ApplyDither)target;
        if (GUILayout.Button("Apply Dither")) {
            //try {
            dither.Apply();
            //}catch(System.Exception e) {
            //    Debug.LogWarning("Error while applying dither: " + e + " Ensure that you have properly generated your dither from GenerateDitherPallate!");
            //}
        }
    }
}
