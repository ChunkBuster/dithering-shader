﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
[CanEditMultipleObjects]
[CustomEditor(typeof(GenerateDither))]
public class DitherEditor : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        GenerateDither dither = (GenerateDither)target;
        
        if (GUILayout.Button("Generate Pallate")) {
            EditorSceneManager.MarkAllScenesDirty();
            dither.GeneratePallate();
            AssetDatabase.SaveAssets();
        }

        if (GUILayout.Button("Generate Texture")) {
            dither.Generate();
        }
    }
}
