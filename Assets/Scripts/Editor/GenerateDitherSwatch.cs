﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class GenerateDitherSwatch : EditorWindow{
    public List<Texture2D> ditherSamples;
    //public GameObject obj = null;
    [MenuItem("Window/Generate Dither Swatch Pallate")]

    public static void ShowWindow() {
        EditorWindow.GetWindow(typeof(GenerateDitherSwatch));
    }

    void OnGUI() {
        ScriptableObject target = this;
        SerializedObject so = new SerializedObject(target);
        SerializedProperty TexturesProperty = so.FindProperty("ditherSamples");
        EditorGUILayout.PropertyField(TexturesProperty, true);
        so.ApplyModifiedProperties();
        if (ditherSamples != null) {
            if(GUILayout.Button("Generate Swatch")) {
                Texture tex = GenerateDitherArray(ditherSamples);
                AssetDatabase.CreateAsset(tex, "Assets/DitherSwatches/Swatch.asset");

                // Print the path of the created asset
                Debug.Log("Created swatch at" + AssetDatabase.GetAssetPath(tex));
            }
        }
           
    }

    private Texture2DArray GenerateDitherArray(List<Texture2D> ditherSamples) {
        Texture2DArray output = new Texture2DArray(16, 16, ditherSamples.Count, TextureFormat.ARGB32, false);
        for (int i = 0; i < ditherSamples.Count; i++)
            output.SetPixels(ditherSamples[i].GetPixels(), i);
        output.filterMode = FilterMode.Point;
        output.wrapMode = TextureWrapMode.Repeat;
        output.Apply();
        return output;
    }
}
