﻿Shader "Custom/Dither" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Main Mask", 2D) = "white" {}
		_RefTex("Reference Mask", 2D) = "white" {}
		_Pallate("Pallate Color", 2D) = "white" {}
		_DitherTextures("Dither Textures", 2DArray) = "" {}
		_DitherDepth("Dither Mask Count", int) = 0
		_DitherScale("Dither Scale multiplier", Range(1,10)) = 1

		// Outline
		_Outline("Outline Extrusion", Range(-1,1)) = 0.05
		_OutColor("Outline Color", Color) = (1,1,1,1)
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

// Outline addition starts here
			Cull Off
			//ZWrite Off
			//ZTest Always // Uncomment for "see through"
			//ZWrite On
			ZWrite Off
			ZTest Always
			ZTest LEqual
			CGPROGRAM
#pragma surface surf Solid vertex:vert
			struct Input {
			float4 color : COLOR;
		};

		fixed4 _OutColor;
		float _Outline;

		fixed4 LightingSolid(SurfaceOutput s, half3 lightDir, half atten) {
			return _OutColor;
		}

		void vert(inout appdata_full v) {
			v.vertex.xyz += v.normal * _Outline * length(ObjSpaceViewDir(v.vertex))
				;
		}

		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = _OutColor.rgb;
		}
		ENDCG

			Cull Back
			ZWrite On
			ZTest LEqual
		// Outline addition ends here
			
		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"
		struct v2f {
		float2 uv : TEXCOORD0;
	};
	v2f vert(
		float4 vertex : POSITION, // vertex position input
		float2 uv : TEXCOORD0, // texture coordinate input
		out float4 outpos : SV_POSITION // clip space position output
	)
	{
		v2f o;
		o.uv = uv;
		outpos = UnityObjectToClipPos(vertex);
		return o;
	}

	UNITY_DECLARE_TEX2DARRAY(_DitherTextures);
	sampler2D _RefTex;
	sampler2D _Pallate;
	float4 _MainTex_ST;
	float _DitherDepth;
	float _DitherScale;
	uniform float4 _Pallate_TexelSize;
	uniform float4 _DitherTextures_TexelSize;
	fixed4 frag(v2f i, UNITY_VPOS_TYPE screenPos : VPOS) : SV_Target
	{
		half4 refCol = tex2D(_RefTex, i.uv);

		half2 screenuv = screenPos;
		half4 returnedCol = half4(refCol);
		returnedCol.a = refCol.a;
		//Select colors from pallate
		float ditherSample = UNITY_SAMPLE_TEX2DARRAY(_DitherTextures,
			float3((screenuv.x / _DitherTextures_TexelSize.z) / (int)_DitherScale,
				(screenuv.y / _DitherTextures_TexelSize.w) / (int)_DitherScale,
				((refCol.b) * 256 * 2))).a;

		if (ditherSample > 0.5) {
			returnedCol = tex2D(_Pallate, half2((refCol.r / (float)_Pallate_TexelSize.w) + (0.9f * (float)_Pallate_TexelSize.x), 0));
		}
		else {
			returnedCol = tex2D(_Pallate, half2((refCol.g / (float)_Pallate_TexelSize.w) + (0.9f  * (float)_Pallate_TexelSize.x), 0));
		}
		returnedCol.a = refCol.a;
		return returnedCol;
	}
		ENDCG
	}


	}
		FallBack "Diffuse"
}
